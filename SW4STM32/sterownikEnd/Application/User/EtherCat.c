#include "EtherCat.h"
#include "xprintf.h"

// inicjalizacja pin�w

void test(void){
	CS_Low();
	HAL_Delay(500);
	CS_High();
	HAL_Delay(500);
}

SyncMode Sync_;
extern PROCBUFFER_OUT BufferOut;
extern PROCBUFFER_IN BufferIn;



unsigned char MainTask()                           // must be called cyclically by the application

{
  uint8_t WatchDog = true;
  uint8_t Operational = false;
  unsigned char i;
  ULONG TempLong;
  unsigned char Status;


  TempLong.Long = SPIReadRegisterIndirect (WDOG_STATUS, 1); // read watchdog status
  if ((TempLong.Byte[0] & 0x01) == 0x01)                    //
    WatchDog = false;                                       // set/reset the corrisponding flag
  else                                                      //
    WatchDog = true;                                        //


  TempLong.Long = SPIReadRegisterIndirect (AL_STATUS, 1);   // read the EtherCAT State Machine status
  Status = TempLong.Byte[0] & 0x0F;                         //
  if (Status == ESM_OP)                                     // to see if we are in operational state
    Operational = true;                                     //
  else                                                      // set/reset the corrisponding flag
    Operational = false;                                    //


                                                            //--- process data transfert ----------
                                                            //
  if (WatchDog | !Operational)                              // if watchdog is active or we are
  {                                                         // not in operational state, reset
    for (i=0; i < TOT_BYTE_NUM_OUT ; i++)                   // the output buffer
    BufferOut.Byte[i] = 0;                                  //

/*                                                          // debug
    if (!Operational)                                       //
      printf("Not operational\n");                          //
    if (WatchDog)                                           //
      printf("WatchDog\n");                                 //
*/                                                          //
  }

  else
  {
    SPIReadProcRamFifo();                                   // otherwise transfer process data from
  }                                                         // the EtherCAT core to the output buffer

  SPIWriteProcRamFifo();                                    // we always transfer process data from
                                                            // the input buffer to the EtherCAT core

  if (WatchDog)                                             // return the status of the State Machine
  {                                                         // and of the watchdog
    Status |= 0x80;                                         //
  }                                                         //
  return Status;                                            //

}







unsigned long SPIReadRegisterDirect (unsigned short Address, unsigned char Len)

                                                   // Address = register to read
                                                   // Len = number of bytes to read (1,2,3,4)
                                                   //
                                                   // a long is returned but only the requested bytes
                                                   // are meaningful, starting from LsByte
{
  ULONG Result;
  UWORD Addr;
  Addr.Word = Address;
  unsigned char i;

  CS_Low();                                             // SPI chip select enable

  SPI_TransferTx(COMM_SPI_READ);                            // SPI read command
  SPI_TransferTx(Addr.Byte[1]);                             // address of the register
  SPI_TransferTxLast(Addr.Byte[0]);                         // to read, MsByte first

  for (i=0; i<Len; i++)                                     // read the requested number of bytes
  {                                                         // LsByte first
    Result.Byte[i] = SPI_TransferRx(DUMMY_BYTE);            //
  }                                                         //

   CS_High();                                           // SPI chip select disable

  return Result.Long;
}


void SPIWriteRegisterDirect (unsigned short Address, unsigned long DataOut)

                                                   // Address = register to write
                                                   // DataOut = data to write
{
  ULONG Data;
  UWORD Addr;
  Addr.Word = Address;
  Data.Long = DataOut;

  CS_Low();                                            // SPI chip select enable

  SPI_TransferTx(COMM_SPI_WRITE);                           // SPI write command
  SPI_TransferTx(Addr.Byte[1]);                             // address of the register
  SPI_TransferTx(Addr.Byte[0]);                             // to write MsByte first

  SPI_TransferTx(Data.Byte[0]);                             // data to write
  SPI_TransferTx(Data.Byte[1]);                             // LsByte first
  SPI_TransferTx(Data.Byte[2]);                             //
  SPI_TransferTxLast(Data.Byte[3]);                         //

  CS_High();                                           // SPI chip select enable
}

unsigned long SPIReadRegisterIndirect (unsigned short Address, unsigned char Len)

                                                   // Address = register to read
                                                   // Len = number of bytes to read (1,2,3,4)
                                                   //
                                                   // a long is returned but only the requested bytes
                                                   // are meaningful, starting from LsByte
{
  ULONG TempLong;
  UWORD Addr;
  Addr.Word = Address;
                                                            // compose the command
                                                            //
  TempLong.Byte[0] = Addr.Byte[0];                          // address of the register
  TempLong.Byte[1] = Addr.Byte[1];                          // to read, LsByte first
  TempLong.Byte[2] = Len;                                   // number of bytes to read
  TempLong.Byte[3] = ESC_READ;                              // ESC read

  SPIWriteRegisterDirect (ECAT_CSR_CMD, TempLong.Long);     // write the command

  do
  {                                                         // wait for command execution
    TempLong.Long = SPIReadRegisterDirect(ECAT_CSR_CMD,4);  //
  }                                                         //
  while(TempLong.Byte[3] & ECAT_CSR_BUSY);                  //


  TempLong.Long = SPIReadRegisterDirect(ECAT_CSR_DATA,Len); // read the requested register
  return TempLong.Long;                                     //
}


void  SPIWriteRegisterIndirect (unsigned long DataOut, unsigned short Address, unsigned char Len)

                                                   // Address = register to write
                                                   // DataOut = data to write
{
  ULONG TempLong;
  UWORD Addr;
  Addr.Word = Address;


  SPIWriteRegisterDirect (ECAT_CSR_DATA, DataOut);            // write the data

                                                              // compose the command
                                                              //
  TempLong.Byte[0] = Addr.Byte[0];                            // address of the register
  TempLong.Byte[1] = Addr.Byte[1];                            // to write, LsByte first
  TempLong.Byte[2] = Len;                                     // number of bytes to write
  TempLong.Byte[3] = ESC_WRITE;                               // ESC write

  SPIWriteRegisterDirect (ECAT_CSR_CMD, TempLong.Long);       // write the command

  do                                                          // wait for command execution
  {                                                           //
    TempLong.Long = SPIReadRegisterDirect (ECAT_CSR_CMD, 4);  //
  }                                                           //
while (TempLong.Byte[3] & ECAT_CSR_BUSY);                   //
}




uint8_t EInit(void)
{




  #define Tout 1000

  ULONG TempLong;
  unsigned short i;

  HAL_Delay(100);
  SPIWriteRegisterDirect (RESET_CTL, DIGITAL_RST);        // LAN9252 reset

  i = 0;                                                  // reset timeout
  do                                                      // wait for reset to complete
  {                                                       //
    i++;                                                  //
    TempLong.Long = SPIReadRegisterDirect (RESET_CTL, 4); //
  }while (((TempLong.Byte[0] & 0x01) != 0x00) && (i != Tout));
                                                          //
  if (i == Tout)                                          // time out expired
  {                                                       //
    return false;                                         // initialization failed
  }

  i = 0;                                                  // reset timeout
  do                                                      // check the Byte Order Test Register
  {                                                       //
    i++;                                                  //
    TempLong.Long = SPIReadRegisterDirect (BYTE_TEST, 4); //
  }while ((TempLong.Long != 0x87654321) && (i != Tout));  //
                                                          //
  if (i == Tout)                                          // time out expired
  {                                                       //
    return false;                                         // initialization failed
  }

  i = 0;                                                  // reset timeout
  do                                                      // check the Ready flag
  {                                                       //
    i++;                                                  //
    TempLong.Long = SPIReadRegisterDirect (HW_CFG, 4);    //
  }while (((TempLong.Byte[3] & READY) == 0) && (i != Tout));//
                                                          //
  if (i == Tout)                                          // time out expired
  {                                                       //
    return false;                                         // initialization failed
  }



#ifdef BYTE_NUM
  xprintf ("STANDARD MODE\n");
#else
  xprintf ("CUSTOM MODE\n");
#endif

  xprintf ("%u Byte Out\n",TOT_BYTE_NUM_OUT);
  xprintf ("%u Byte In\n",TOT_BYTE_NUM_IN);

  xprintf ("Sync = ");


  if ((Sync_ == DC_SYNC) || (Sync_ == SM_SYNC))           //--- if requested, enable --------
  {                                                       //--- interrupt generation --------

    if (Sync_ == DC_SYNC)
    {                                                     // enable interrupt from SYNC 0
      SPIWriteRegisterIndirect (0x00000004, AL_EVENT_MASK, 4);
                                                          // in AL event mask register, and disable
                                                          // other interrupt sources
      xprintf("DC_SYNC\n");
    }

    else
    {                                                     // enable interrupt from SM 0 event
      SPIWriteRegisterIndirect (0x00000100, AL_EVENT_MASK, 4);
                                                          // in AL event mask register, and disable
                                                          // other interrupt sources
      xprintf("SM_SYNC\n");
    }

    SPIWriteRegisterDirect (IRQ_CFG, 0x00000111);         // set LAN9252 interrupt pin driver
                                                          // as push-pull active high
                                                          // (On the EasyCAT shield board the IRQ pin
                                                          // is inverted by a mosfet, so Arduino
                                                          // receives an active low signal)

    SPIWriteRegisterDirect (INT_EN, 0x00000001);          // enable LAN9252 interrupt
  }

  else
  {
    xprintf("ASYNC\n");
  }



  TempLong.Long = SPIReadRegisterDirect (ID_REV, 4);      // read the chip identification
   xprintf ("Detected chip ");                              // and revision, and print it
   xprintf ("%x ", TempLong.Word[1]);                       // out on the serial line
   xprintf (" Rev ");                                       //
   xprintf ("%u \n", TempLong.Word[0]);                     //





  return true;

}


void SPIReadProcRamFifo()    // read BYTE_NUM bytes from the output process ram, through the fifo
                                      //
                                      // these are the bytes received from the EtherCAT master and
                                      // that will be use by our application to write the outputs
{
  ULONG TempLong;
  unsigned char i;

  #if TOT_BYTE_NUM_OUT > 0

    SPIWriteRegisterDirect (ECAT_PRAM_RD_CMD, PRAM_ABORT);        // abort any possible pending transfer


    SPIWriteRegisterDirect (ECAT_PRAM_RD_ADDR_LEN, (0x00001000 | (((uint32_t)TOT_BYTE_NUM_OUT) << 16)));
                                                                  // the high word is the num of bytes
                                                                  // to read 0xTOT_BYTE_NUM_OUT----
                                                                  // the low word is the output process
                                                                  // ram offset 0x----1000

    SPIWriteRegisterDirect (ECAT_PRAM_RD_CMD, 0x80000000);        // start command


                                                //------- one round is enough if we have ----
                                                //------- to transfer up to 64 bytes --------

    do                                                            // wait for the data to be
    {                                                             // transferred from the output
      TempLong.Long = SPIReadRegisterDirect (ECAT_PRAM_RD_CMD,2); // process ram to the read fifo
    }                                                             //
    while (TempLong.Byte[1] != FST_LONG_NUM_OUT);                 //

    CS_Low();                                                // enable SPI chip select

    SPI_TransferTx(COMM_SPI_READ);                                // SPI read command
    SPI_TransferTx(0x00);                                         // address of the read
    SPI_TransferTxLast(0x00);                                     // fifo MsByte first

    for (i=0; i< FST_BYTE_NUM_ROUND_OUT; i++)                     // transfer the data
    {                                                             //
      BufferOut.Byte[i] = SPI_TransferRx(DUMMY_BYTE);             //
    }                                                             //

    CS_High();                                                // disable SPI chip select

   #endif



}



void SPIWriteProcRamFifo()    // write BYTE_NUM bytes to the input process ram, through the fifo
                                       //
                                    // these are the bytes that we have read from the inputs of our
                                       // application and that will be sent to the EtherCAT master
{

  ULONG TempLong;
  unsigned char i;
  uint8_t j=20;


  #if TOT_BYTE_NUM_IN > 0

    SPIWriteRegisterDirect (ECAT_PRAM_WR_CMD, PRAM_ABORT);        // abort any possible pending transfer


    SPIWriteRegisterDirect (ECAT_PRAM_WR_ADDR_LEN, (0x00001200 | (((uint32_t)TOT_BYTE_NUM_IN) << 16)));
                                                                  // the high word is the num of bytes
                                                                  // to write 0xTOT_BYTE_NUM_IN----
                                                                  // the low word is the input process
                                                                  // ram offset  0x----1200

    SPIWriteRegisterDirect (ECAT_PRAM_WR_CMD, 0x80000000);        // start command


                                                //------- one round is enough if we have ----
                                                //------- to transfer up to 64 bytes --------

    do                                                            // check that the fifo has
    {                                                             // enough free space
      TempLong.Long = SPIReadRegisterDirect (ECAT_PRAM_WR_CMD,2); //
    }                                                             //
    while (TempLong.Byte[1] < FST_LONG_NUM_IN);                   //

    CS_Low();                                                 // enable SPI chip select

    SPI_TransferTx(COMM_SPI_WRITE);
    SPI_TransferTx(0x00);// SPI write command
    SPI_TransferTx(0x20);                                         // MsByte first

    for (i=0; i< (FST_BYTE_NUM_ROUND_IN - 1 ); i++)               // transfer the data
    {                                                             //
      SPI_TransferTx (BufferIn.Byte[i]);                          //
    }                                                             //
                                                                  //
    SPI_TransferTxLast (BufferIn.Byte[i]);                        // one last byte

    CS_High();                                                // disable SPI chip select
  #endif

#if SEC_BYTE_NUM_IN > 0                     //-- if we have to transfer more then 64 bytes --
                                              //-- we must do another round -------------------
                                              //-- to transfer the remainig bytes -------------

    do                                                          // check that the fifo has
    {                                                           // enough free space
      TempLong.Long = SPIReadRegisterDirect(ECAT_PRAM_WR_CMD,2);//
    }                                                           //
    while (TempLong.Byte[1] < (SEC_LONG_NUM_IN));               //

    SCS_Low_macro                                               // enable SPI chip select

    SPI_TransferTx(COMM_SPI_WRITE);                             // SPI write command
    SPI_TransferTx(0x00);                                       // address of the write fifo
    SPI_TransferTx(0x20);                                       // MsByte first

    for (i=0; i< (SEC_BYTE_NUM_ROUND_IN - 1); i++)              // transfer loop for the remaining
    {                                                           // bytes
      SPI_TransferTx (BufferIn.Byte[i+64]);                     // we transfer the second part of
    }                                                           // the buffer, so offset by 64
                                                                //
    SPI_TransferTxLast (BufferIn.Byte[i+64]);                   // one last byte

    SCS_High_macro                                              // disable SPI chip select
  #endif




}








